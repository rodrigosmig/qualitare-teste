<?php namespace Qualitare\Test\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Classrooms Back-end Controller
 */
class Classrooms extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Qualitare.Test', 'test', 'classrooms');
    }
}
