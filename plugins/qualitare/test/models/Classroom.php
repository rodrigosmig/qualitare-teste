<?php namespace Qualitare\Test\Models;

use Model;

/**
 * Class Model
 */
class Classroom extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'qualitare_test_classrooms';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
