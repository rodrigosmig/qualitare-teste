<?php namespace Qualitare\Test\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateClassroomsTable extends Migration
{
    public function up()
    {
        Schema::create('qualitare_test_classrooms', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->integer('shift');
            $table->integer('grade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('qualitare_test_classes');
    }
}
