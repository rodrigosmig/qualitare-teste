# Teste de suporte em desenvolvimento

## Dependências

MySQL, PHP 7+, Composer e OctoberCMS (Laravel)

## Inicializando

1. Certifique-se que o sistema operacional possui a versão do PHP 7 ou superior (7.2 de preferência). Baixe também o [**Composer**](https://getcomposer.org/), o mesmo será necessário para instalar as dependências do projeto.
2. Clone este repositório, e logo após acesse via Terminal (ou Prompt de Comando) o diretório do mesmo.
3. Execute o comando `composer install` para instalar todas as depedências.
4. Copie o arquivo `.env.example` para `.env`, localizado na raiz do projeto.
5. Crie um banco de dados MySQL, e modifique o arquivo `.env` com as configurações do recém criado banco.
6. Execute o comando `php artisan october:up` em seu Terminal para realizar as configurações iniciais do OctoberCMS.
7. Inicie o servidor utilizando o comando `php artisan serve --host=0.0.0.0 --port=5000`, e então acesso o serviço através do endereço mostrado (http://localhost:5000)

## October CMS

O October CMS é um Framework construído em cima do Laravel, provendo um painel administrativo bastante eficiente. Familiarize-se um pouco lendo sua [documentação](https://octobercms.com/docs/database/model).

### Linha de comando

É possível criar Models, Controllers e outros objetos através da linha de comando, acesse a [documentação de comandos](https://octobercms.com/docs/console/commands) para maiores informações. Boa parte dos comandos devem informar nome de ambos o Autor e Plugin, segue abaixo:

**Author**: Qualitare
**Plugin**: Test

Exemplo de criação de Model:
`php artisan create:model Qualitare.Test Student`

## Objetivo

1. Criar um *Model* chamado **Student**;
2. Criar um *Controller* chamado **Students**;
3. Criar campos do banco de dados utilizando *Migration* ou via *Plugin Builder* do *October*;
4. Criar relação entre Aluno (**Student**) e Turma (**Classroom**);
5. Criar campos do formulário do controlador utilizando o *Plugin Builder* do *October*;
6. Criar listagem do *Model* de **Student**, exibindo *Nome*, *Email* e *Turma* (**Classroom**).

### Model Student

Segue abaixo os campos do Model:

- name: VARCHAR
- email: VARCHAR
- age: INT
- classroom_id: INT
- created_at: TIMESTAMP
- updated_at: TIMESTAMP

### Plugin Builder
O October disponibiliza um plugin que auxilia na criação de **Models**, **Controllers** e **Formulários**. O uso deste plugin é opcional, pois é possível realizar a criação de todos os objetos através do código.
